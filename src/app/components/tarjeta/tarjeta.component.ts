import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,  Validators  } from '@angular/forms';
import { UsuarioDataI } from '../interfaces/usuario.interfaces';
import { MatTableDataSource} from '@angular/material/table';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.css']
})
export class TarjetaComponent implements OnInit {
  hide = true;
  listUsuarios: UsuarioDataI[] = [];
  displayedColumns: string[] = ['usuario', 'nombre', 'apellido', 'sexo','acciones'];
 dataSource!: MatTableDataSource<any>;
  form!: FormGroup
  listTarjetas: any [] = [
    {titular: 'Lucas Alagaiaga', numeroTarjeta:'2244223366778822', fechaExpiracion:'01/20',cvv:'123' },
    {titular: 'Juan Carlos', numeroTarjeta:'1234232323232323', fechaExpiracion:'05/21',cvv:'312' },
    {titular: 'Tomas Ruiz Diaz', numeroTarjeta:'3322667788553322', fechaExpiracion:'11/22',cvv:'231' },
    {titular: 'Maria Cortes', numeroTarjeta:'1234567423232323', fechaExpiracion:'06/25',cvv:'213' }
  ]
 

  constructor(private fb: FormBuilder,
    private _usuarioservice:UsuarioService,
              private _snackbar: MatSnackBar) {
    this.form = this.fb.group({
      titular:['', [Validators.required, Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      numeroTarjeta:['',[Validators.required,Validators.minLength(12),Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      fechaExpiracion:['',[Validators.required, Validators.maxLength(5)]],
      cvv:['',[Validators.required, Validators.maxLength(3)]]
    })
    
  }

  ngOnInit(): void {
  }
  crearFormulario():void{
    this.form = this.fb.group({
      nombre:['',Validators.required],
      numTarjeta:['',[Validators.required,Validators.minLength(12),Validators.maxLength(12)]],
      fechaActual:['',[Validators.required,Validators.minLength(5),Validators.maxLength(5)]],
      position:['',[Validators.required,Validators.minLength(3),Validators.maxLength(3)]],
    })
  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService?.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
  }


  crearTarjeta(){
    const user : UsuarioDataI = {
      nombre: this.form.value.nombre,
      position: this.form.value.position,
      numTarjeta: this.form.value.numTarjeta,
      fechaActual: this.form.value.fechaActual,
    }

    this._usuarioservice.agregarUsuario(user);
    console.log(this.form.value);
    console.log(user);
    

    this._snackbar.open('El usuario fue agregado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
     });
      
    }


}
