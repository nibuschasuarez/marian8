import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../components/interfaces/usuario.interfaces';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  listUsuarios: any;

  constructor() { }
  getUsuario():UsuarioDataI[]{
    return this.listUsuarios.slice();

  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }

  agregarUsuario(usuario:UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
    console.log(usuario);
    
  }
}
